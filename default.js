// Function to radd undefined properties in an object

function defaults(obj,defaultProps) {

    for(prop in defaultProps ){
        if(!obj.hasOwnProperty(prop)){

            obj[prop]=defaultProps[prop]
        }
    }

    return obj;
}



module.exports = defaults ;